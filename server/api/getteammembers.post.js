import sdk from 'node-appwrite'
import { API_KEY } from '../../config.json'

export default (async (event) => {
    const client = new sdk.Client();
    const users = new sdk.Users(client);

    client
      .setEndpoint('https://appwrite.xeovalyte.com/v1') // Your API Endpoint
      .setProject('62f0f5758fa678b3e1c9') // Your project ID
      .setKey(API_KEY) // Your secret API key

    const body = await useBody(event)

    let members = [];

   const promises = body.members.map(async (userId) => {
      const user = await users.get(userId);
      members.push(user)
    })

    await Promise.all(promises)

    return {
      members: members
    }
  })
  