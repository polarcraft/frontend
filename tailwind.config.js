/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "app.vue",
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#BAE5F5',
          200: '#A8DFF2',
          300: '#97D8F0',
          400: '#85D2ED',
          500: '#74CBEB',
          600: '#66B2CE',
          700: '#5798B0',
          800: '#487F93',
          900: '#3A6675',
        },
        dark: {
          100: '#537986',
          200: '#3A6675',
          300: '#335966',
          400: '#2B4D58',
          500: '#244049',
          600: '#1D333A',
          700: '#16262C',
          800: '#0E1A1D',
          900: '#070D0F',
        },
        light: {
          100: '#F6FCFE',
          200: '#EEF8FC',
          300: '#E5F5FB',
          400: '#DCF2FA',
          500: '#D4EFF9',
          600: '#CBEBF8',
          700: '#C3E8F6',
          800: '#BAE5F5',
          900: '#A3C8D6',
        }
      }
    },
  },
  plugins: [],
}
